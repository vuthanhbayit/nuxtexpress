'use strict';
module.exports = (sequelize, DataTypes) => {
    var Task = sequelize.define('task', {
        title: DataTypes.STRING
    });

    Task.associate = models => {
        models.task.belongsTo(models.user, {
            onDelete: "CASCADE",
            foreignKey: {
                allowNull: false
            }
        });
    };

    return Task;
};
