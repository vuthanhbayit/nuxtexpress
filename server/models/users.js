'use strict';
module.exports = (sequelize, DataTypes) => {
    var User = sequelize.define('user', {
        username: DataTypes.STRING
    });

    User.associate = models => {
        models.user.hasMany(models.task);
    };

    return User;
};
