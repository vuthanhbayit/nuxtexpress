'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('Users', [
            {
                username: 'SuperAdmin',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                username: 'Admin',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                username: 'Tester',
                createdAt: new Date(),
                updatedAt: new Date()
            },
        ], {});
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('Users', null, {});
    }
};
