const express = require('express')
const router = express.Router()

//api users
router.use('/users', require('./users.route'))

module.exports = router
