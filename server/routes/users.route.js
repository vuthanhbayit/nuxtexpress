const {Router} = require('express')
const router = Router()
const models  = require('../models');

router.get('/', function(req, res) {
    models.user.findAll({
        include: [ models.task ],
        attributes: ['id', 'username']
    }).then(users => {
        res.json(users);
    });
});

module.exports = router
