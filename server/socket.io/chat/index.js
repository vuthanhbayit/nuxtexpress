module.exports.run = (io, socket) => {
    socket.on('send-message', message => {
        socket.broadcast.emit('new-message', message)
        io.sockets.emit('sendAll', message)
    })
}
