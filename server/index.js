require('dotenv').config()

const express = require('express')
const consola = require('consola')
const logger = require('morgan');
const bodyParser = require('body-parser');
const {Nuxt, Builder} = require('nuxt')

const app = express()

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// Require API routes
const routes = require('./routes')
app.use('/api', routes)

// Import and Set Nuxt.js options
const config = require('../nuxt.config.js')
config.dev = process.env.NODE_ENV !== 'production'

async function start() {
    // Init Nuxt.js
    const nuxt = new Nuxt(config)

    const {host, port} = nuxt.options.server

    // Build only in dev mode
    if (config.dev) {
        const builder = new Builder(nuxt)
        await builder.build()
    } else {
        await nuxt.ready()
    }

    // Give nuxt middleware to express
    app.use(nuxt.render)

    // Listen the server
    const server = app.listen(port, host)
    consola.ready({
        message: `Server listening on http://${host}:${port}`,
        badge: true
    })
    socketStart(server)
    console.log('Socket.IO starts')
}

function socketStart(server) {
    const io = require('socket.io').listen(server)
    io.on('connection', socket => {
        console.log('id: ' + socket.id + ' is connected')

        require('./socket.io').run(io, socket)

        socket.on('disconnect', () => {
            console.log('id: ' + socket.id + ' is disconnected')
        })
    })
}

start()
